Tässä repossa on muutama koulutöissä tekemiäni koodeja. 

Otin koodit kahdelta kurssilta: FYS-4096 Computational Physics ja SGN-41007 Pattern Recognition and Machine Learning.

Laskennallisessa fysiikassa on tekemäni koodit kurssin harjoitustöihin. Ensimmäisessä projektissa on mallinnettu aallon törmäämistä esteeseen, 
toisessa projektissa on mallinnettu magnetoitumista.

Jälkimmäisessä kurssissa oli kuvantunnistustehtävä, hyödynsin siinä valmiiksi koulutettuja neuroverkkoja.